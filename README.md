## Vue.js Dashboard
**Author: Beta Priyoko**

---

## How to use with development server

Make sure you have internet connection because some of assets is from cdn and you've install node.js in your pc/laptop/quantum computer;

1. Clone this repository 
2. Go to project folder
3. open terminal/command prompt, then type `npm install` then hit **enter**
4. after that type `npm run serve`
5. open your browser and goto `http://localhost:8080`

---

## How to use with web server (`http-server`)

1. Clone this repository 
2. Go to project folder
3. open terminal/command prompt, then type `npm install` then hit **enter**
4. install **http-server** by typing `npm install -g http-server`
5. Run `http-server dist/` and then hit **enter**

---

